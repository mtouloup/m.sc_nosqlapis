package com.my.nosql;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.gt;
import static com.mongodb.client.model.Filters.gte;
import static com.mongodb.client.model.Filters.lt;
import static com.mongodb.client.model.Filters.lte;
import static com.mongodb.client.model.Updates.set;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Delete;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.filter.BinaryComparator;
import org.apache.hadoop.hbase.filter.CompareFilter.CompareOp;
import org.apache.hadoop.hbase.filter.Filter;
import org.apache.hadoop.hbase.filter.FilterList;
import org.apache.hadoop.hbase.filter.FilterList.Operator;
import org.apache.hadoop.hbase.filter.PrefixFilter;
import org.apache.hadoop.hbase.filter.QualifierFilter;
import org.bson.Document;
import org.bson.conversions.Bson;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;

/**
 * Java MongoDB Connector APIs
 */
public class ConnectorApp {

	/** True if Mongo is connected */
	private boolean isMongoConnected;

	/** Mongo client object */
	private MongoClient mongo;

	/** The Mongo database to connect */
	private MongoDatabase database;

	/** True if HBase is connected */
	private boolean isHBaseConnected;

	/** The HBase connection */
	private Connection connection;

	/**
	 * Connect to Mongo database on the specified host and port.
	 *
	 * @param host
	 *            the Mongo host to connect
	 * @param port
	 *            the Mongo port to connect
	 * @param dbname
	 *            the Mongo database to connect
	 * @return result the status of mongo connection.
	 */
	public boolean connectMongo(String host, int port, String dbname) {
		boolean result = false;

		/**** Connect to MongoDB ****/
		mongo = new MongoClient(host, port);

		// if database does not exist, MongoDB will create it
		database = mongo.getDatabase(dbname);
		result = true;

		setMongoConnected(result);
		return result;
	}

	/**
	 * Connects to Mongo database on the specified host and port.
	 *
	 * @param url
	 *            the url to connect Mongo
	 * @return result the status for Mongo connection.
	 */
	public boolean connectMongo(String url) {
		boolean result = false;

		/**** Connect to MongoDB ****/
		mongo = new MongoClient(new MongoClientURI(url));

		setMongoConnected(result);
		return result;
	}

	/**
	 * Connects to HBase on the specified host port and data path.
	 *
	 * @param host
	 *            the HBase host to connect
	 * @param port
	 *            the HBase port to connect
	 * @param path
	 *            the HBase data folder
	 * @return result the status for hbase connection.
	 */
	public boolean connectHBase(String host, int port, String path) {
		boolean result = false;

		Configuration config = HBaseConfiguration.create();

		config.set("hbase.zookeeper.quorum", host);
		config.set("hbase.zookeeper.property.clientPort", String.valueOf(port));
		config.set("zookeeper.znode.parent", path);

		try {
			connection = ConnectionFactory.createConnection(config);
			result = true;
			System.out.println("HBase is running!");
		} catch (IOException e) {
			e.printStackTrace();
		}

		setHBaseConnected(result);
		return result;
	}

	/**
	 * Close Mongo connection.
	 */
	public void disconnectMongo() {
		if (mongo != null) {
			mongo.close();
		}
		setMongoConnected(false);
	}

	/**
	 * Close HBase connection.
	 */
	public void disconnectHBase() {
		try {
			if (connection != null) {
				connection.close();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		setHBaseConnected(false);
	}

	/**
	 * Getter for isMongoConnected
	 * 
	 * @return the isMongoConnected
	 */
	public boolean isMongoConnected() {
		return isMongoConnected;
	}

	/**
	 * Setter for isMongoConnected to set.
	 *
	 * @param isMongoConnected
	 *            the isMongoConnected to set
	 */
	public void setMongoConnected(boolean isMongoConnected) {
		this.isMongoConnected = isMongoConnected;
	}

	/**
	 * Getter for isHBaseConnected
	 * 
	 * @return the isHBaseConnected
	 */
	public boolean isHBaseConnected() {
		return isHBaseConnected;
	}

	/**
	 * Setter for isHBaseConnected to set.
	 *
	 * @param isHBaseConnected
	 *            the isHBaseConnected to set
	 */
	public void setHBaseConnected(boolean isHBaseConnected) {
		this.isHBaseConnected = isHBaseConnected;
	}
}
