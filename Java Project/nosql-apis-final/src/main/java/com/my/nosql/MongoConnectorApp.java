package com.my.nosql;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.gt;
import static com.mongodb.client.model.Filters.gte;
import static com.mongodb.client.model.Filters.lt;
import static com.mongodb.client.model.Filters.lte;
import static com.mongodb.client.model.Updates.set;
import org.bson.Document;
import org.bson.conversions.Bson;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import com.mongodb.util.JSON;
import com.mongodb.BasicDBObject;

/**
 * Java MongoDB Connector APIs
 */
public class MongoConnectorApp {

    /** True is Mongo is connected */
    private static boolean isMongoConnected;

    /** The Mongo client object */
    private static MongoClient mongo;

    /** The Mongo database to connect */
    private MongoDatabase database;

    /**
     * Connects to Mongo database on the specified host and port.
     *
     * @param host
     *            the Mongo host to connect
     * @param port
     *            the Mongo port to connect
     * @param dbname
     *            the Mongo database to connect
     * @return result the status for mongo connection.
     */
    public boolean connectMongo(String host, int port, String dbname) {
        boolean result = false;

        /**** Connect to MongoDB ****/
        mongo = new MongoClient(host, port);

        // if database doesn't exists, MongoDB will create it for you
        database = mongo.getDatabase(dbname);
        result = true;

        setMongoConnected(result);
        return result;
    }

    /**
     * Connects to Mongo database on the specified host and port.
     *
     * @param url
     *            the url to connect Mongo
     * @return result the status for Mongo connection.
     */
    public boolean connectMongo(String url) {
        boolean result = false;

        /**** Connect to MongoDB ****/
        mongo = new MongoClient(new MongoClientURI(url));

        setMongoConnected(result);
        return result;
    }

    /**
     * Shutdowns Mongo connection.
     */
    public void disconnectMongo() {
        if (mongo != null) {
            mongo.close();
        }
        setMongoConnected(false);
    }

    /**
     * Getter for isMongoConnected
     * 
     * @return the isMongoConnected
     */
    public static boolean isMongoConnected() {
        return isMongoConnected;
    }

    /**
     * Setter for isMongoConnected to set.
     *
     * @param isMongoConnected
     *            the isMongoConnected to set
     */
    public void setMongoConnected(boolean isMongoConnected) {
        this.isMongoConnected = isMongoConnected;
    }

    /**
     * Filters the collections and returns result.
     *
     * @param collectionName
     *            the name of the collection to filter
     * @return documents the result set on filter
     */
    public FindIterable<Document> filter(String collectionName) {
        if (!isMongoConnected())
            return null;

        // if collection doesn't exists, MongoDB will create it for you
        MongoCollection<Document> collection = database.getCollection(collectionName);

        /**** Find and display ****/
        FindIterable<Document> documents = collection.find();

        return documents;
    }

    /**
     * Filters the collections and returns result.
     *
     * @param collectionName
     *            the name of the collection to filter
     * @param filter
     *            the filter condition on the collection to filter
     * @return documents the result set on filter
     */
    public FindIterable<Document> filter(String collectionName, Bson filter) {
        if (!isMongoConnected())
            return null;

        // if collection doesn't exists, MongoDB will create it for you
        MongoCollection<Document> collection = database.getCollection(collectionName);

        /**** Find and display ****/
        FindIterable<Document> documents = collection.find(filter);

        return documents;
    }

    /**
     * Updates the collection and return result.
     *
     * @param collectionName
     *            the name of the collection to filter
     * @return result the result for update
     */
    public UpdateResult update(String collectionName, Bson filter, Bson update) {
        if (!isMongoConnected())
            return null;

        // if collection doesn't exists, MongoDB will create it for you
        MongoCollection<Document> collection = database.getCollection(collectionName);

        /**** Update ****/
        UpdateResult result = collection.updateOne(filter, update);

        return result;
    }

    /**
     * Deletes from collection and return result.
     *
     * @param collectionName
     *            the name of the collection to filter
     * @return result the result of deletion
     */
    public DeleteResult delete(String collectionName, Bson filter) {
        if (!isMongoConnected())
            return null;

        // if collection doesn't exists, MongoDB will create it for you
        MongoCollection<Document> collection = database.getCollection(collectionName);

        /**** Delete ****/
        DeleteResult result = collection.deleteOne(filter);

        return result;
    }

    public static void collectionNames(String db) {
        if (!isMongoConnected())
            System.out.println("Mongo is not Connected");

        try {
            MongoDatabase database = mongo.getDatabase(db);

            MongoIterable<String> collections = database.listCollectionNames();
            for (String collectionName : collections) {
                System.out.println(collectionName);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean createDatabaseWithCollection(String databaseName, String collection) {
        boolean result = false;
        if (!isMongoConnected())
            System.out.println("Mongo is not Connected");
        try {
            MongoDatabase database = mongo.getDatabase(databaseName);

            Document document = new Document();
            document.put("Continent1", "{_id : 1, name : 'Africa'}");

            database.getCollection(collection).insertOne(document);

            result = true;

        } catch (Exception e) {
            System.out.println("Error " + e.getMessage());

        }
        return result;
    }

    public static boolean AddDataToCollection(String db, String collection, Document document) {

        boolean result = false;

        if (!isMongoConnected())
            System.out.println("Mongo is not Connected");
        ;

        MongoDatabase database = mongo.getDatabase(db);

        MongoCollection<Document> contCol = database.getCollection(collection);

        try {
            contCol.insertOne(document);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    /**
     * Main method to invoke the application.
     *
     * @param args
     *            the command line arguments
     */
    public static void main(String[] args) {
        Mongo();
    }

    /**
     * API Tests for MongoDb
     */
    private static void Mongo() {

        /** Declare the app object **/
        MongoConnectorApp app = new MongoConnectorApp();

        /** Mongo configuration parameters **/
        final String host = "localhost";
        final int port = 27017;
        final String dbname = "Music";
        final String collectionName = "2018";

        /** Declaring Filters in Bson format **/
        final Bson filter = eq("record", "This is a Test"); // Equals =
        final Bson filtergt = gt("ID", 1); // Greater than >
        final Bson filtergte = gte("ID", 1); // Greater than >=
        final Bson filterlt = lt("ID", 5); // Less than equal <
        final Bson filterlte = lte("ID", 5); // Less than equal <=

        /** Create the Connection to MongoDb **/
        app.connectMongo(host, port, dbname);

        /** Create Database with Collection **/
        /*
         * System.out.println("Database Created? " +
         * app.createDatabaseWithCollection(dbname, collectionName));
         */
        
        /** Add document to collection **/
        /*
         * Document document = new Document(); document.put("Continent1",
         * "{_id : 1, name : 'Africa'}"); System.out.println("Documents Inserted: "+
         * app.AddDataToCollection(dbname,collectionName,document));
         */

        /**** Find and display All ****/
        /*
         * FindIterable<Document> documents = app.filter(collectionName); for (Document
         * doc : documents) { System.out.println(doc); }
         */

        /**** Find and display All collections from db ****/

        collectionNames(dbname);

        /**** Find and display on filter ****/
        /*
         * FindIterable<Document> filterDocuments = app.filter(collectionName, filter);
         * for (Document doc : filterDocuments) { System.out.println(doc); }
         */

        /**** Update ****/
        /*
         * Bson update = set("record", "This is a Test"); UpdateResult result =
         * app.update(collectionName, filter, update);
         * System.out.println(result.getModifiedCount());
         */

        /**** Delete ****/
        /*
         * DeleteResult delresult = app.delete(collectionName, filter);
         * System.out.println(delresult.getDeletedCount());
         */

        app.disconnectMongo();
    }
}
