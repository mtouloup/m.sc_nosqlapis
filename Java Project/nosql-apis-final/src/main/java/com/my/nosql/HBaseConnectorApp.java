package com.my.nosql;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Delete;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.filter.BinaryComparator;
import org.apache.hadoop.hbase.filter.CompareFilter.CompareOp;
import org.apache.hadoop.hbase.filter.Filter;
import org.apache.hadoop.hbase.filter.FilterList;
import org.apache.hadoop.hbase.filter.FilterList.Operator;
import org.apache.hadoop.hbase.filter.PrefixFilter;
import org.apache.hadoop.hbase.filter.QualifierFilter;
import org.apache.hadoop.hbase.util.Bytes;

/**
 * Java HBase Connector APIs
 */
public class HBaseConnectorApp {

	/** True if HBase is connected */
	private static boolean isHBaseConnected;

	/** The HBase connection */
	private static Connection connection;

	/**
	 * Connects to HBase on the specified host and port.
	 *
	 * @param host
	 *            the HBase host to connect
	 * @param port
	 *            the HBase port to connect
	 * @param path
	 *            the HBase data folder
	 * @return result the status for mongo connection.
	 */
	public boolean connectHBase(String host, int port, String path) {
		boolean result = false;

		Configuration config = HBaseConfiguration.create();

		config.set("hbase.zookeeper.quorum", host);
		config.set("hbase.zookeeper.property.clientPort", String.valueOf(port));
		config.set("zookeeper.znode.parent", path);

		try {
			connection = ConnectionFactory.createConnection(config);
			result = true;
			System.out.println("HBase is running!");
		} catch (IOException e) {
			e.printStackTrace();
		}

		setHBaseConnected(result);
		return result;
	}

	/**
	 * Close HBase connection.
	 */
	public void disconnectHBase() {
		try {
			if (connection != null) {
				connection.close();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		setHBaseConnected(false);
	}

	/**
	 * Getter for isHBaseConnected
	 * 
	 * @return the isHBaseConnected
	 */
	public static boolean isHBaseConnected() {
		return isHBaseConnected;
	}

	/**
	 * Setter for isHBaseConnected to set.
	 *
	 * @param isHBaseConnected
	 *            the isHBaseConnected to set
	 */
	public void setHBaseConnected(boolean isHBaseConnected) {
		this.isHBaseConnected = isHBaseConnected;
	}

	/**
	 * Filters the table and returns result.
	 *
	 * @param tableName
	 *            the name of the table to filter
	 * @return scanner the result set on filter
	 */
	public ResultScanner filter(TableName tableName) {
		if (!isHBaseConnected())
			return null;

		ResultScanner scanner = null;
		// Retrieve the table so we can do some reads
		try {
			Table table = connection.getTable(tableName);
			Scan scan = new Scan();
			scanner = table.getScanner(scan);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return scanner;
	}

	/**
	 * Filter the collections and returns result.
	 *
	 * @param tableName
	 *            the name of the table to filter
	 * @param filters
	 *            the list of filter conditions
	 * @return scanner the result set on filter
	 */
	public ResultScanner filter(TableName tableName, List<Filter> filters) {
		if (!isHBaseConnected())
			return null;

		Scan scan = new Scan();
		scan.setFilter(new FilterList(Operator.MUST_PASS_ONE, filters));

		ResultScanner scanner = null;
		// Retrieve the table so we can do some reads
		try {
			Table table = connection.getTable(tableName);
			scanner = table.getScanner(scan);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return scanner;
	}

	/**
	 * Update the collection and return result.
	 *
	 * @param collectionName
	 *            the name of the collection to filter
	 * @return result the result for update
	 */
	public boolean update(TableName tableName, String rowId, String family, String qualifier, String value) {
		boolean result = false;

		if (!isHBaseConnected())
			return result;

		Put put = new Put(rowId.getBytes());
		put.addColumn(family.getBytes(), qualifier.getBytes(), value.getBytes());

		Delete delete = new Delete(rowId.getBytes());
		delete.addColumn(family.getBytes(), qualifier.getBytes());

		try {
			Table table = connection.getTable(tableName);
			table.put(put);
			result = true;
		} catch (IOException e) {
			e.printStackTrace();
		}

		return result;
	}

	/**
	 * Deletes from table and return result.
	 *
	 * @param TableName
	 *            the name of the collection to filter
	 * @return result the result of deletion
	 */
	public boolean delete(TableName tableName, String rowId, String family, String qualifier) {
		boolean result = false;
		if (!isHBaseConnected())
			return result;

		Delete delete = new Delete(rowId.getBytes());
		delete.addColumn(family.getBytes(), qualifier.getBytes());

		try {
			Table table = connection.getTable(tableName);
			table.delete(delete);
			result = true;
		} catch (IOException e) {
			e.printStackTrace();
		}

		return result;
	}

	/**
	 * Populate and lists all existed tables.
	 */
	public static HTableDescriptor[] listTable() {

		if (!isHBaseConnected())
			System.out.println("Hbase is Connected");
		HTableDescriptor[] tableDescriptors = null;
		try {
			Admin admin = connection.getAdmin();
			tableDescriptors = admin.listTables();
			for (HTableDescriptor tableDescriptor : tableDescriptors) {
				System.out.println("Table Name:" + tableDescriptor.getNameAsString());
			}
			admin.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return tableDescriptors;
	}

	/**
	 * Deletes an existing table.
	 *
	 * @param TableName
	 *            the name of the table to be deleted
	 */
	public boolean DeleteTable(TableName tableName) {

		boolean result = false;
		if (!isHBaseConnected())
			return result;

		Connection connection = null;
		Admin admin = null;

		try {
			connection = ConnectionFactory.createConnection(HBaseConfiguration.create());
			admin = connection.getAdmin();
			admin.disableTable(tableName);
			admin.deleteTable(tableName);
			if (!admin.tableExists(tableName)) {
				result = true;
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (admin != null)
					admin.close();
				if (connection != null)
					connection.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	/**
	 * Create a table if not exists and return true for success and false for
	 * failure.
	 *
	 * @param TableName
	 *            the name of the table to create
	 * @param family
	 *            the name of the family on the created table
	 */
	public boolean CreateTable(TableName tableName, String family) {

		boolean result = false;
		if (!isHBaseConnected())
			return result;

		try {
			Admin admin = connection.getAdmin();

			if (!admin.tableExists(tableName)) {
				HTableDescriptor htable = new HTableDescriptor(tableName);
				htable.addFamily(new HColumnDescriptor(family));
				admin.createTable(htable);
				result = true;
			} else {
				result = false;
			}
			admin.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	/**
	 * Main method to invoke the application.
	 *
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String[] args) {
		HBase();
	}

	/**
	 * API Tests for HBase
	 */
	private static void HBase() {
		/** HBase config **/
		final String host = "myhbase";
		final int port = 2181;
		final String path = "/hbase";

		/** Declare the app object **/
		HBaseConnectorApp app = new HBaseConnectorApp();
		
		/** Connect to HBase **/
		app.connectHBase(host, port, path);

		/** Declare the Table Name **/
		TableName tableName = TableName.valueOf("cars");

		/** Filtering data HBase **/
		String rowId = "row1";
		String family = "vi";
		String qualifier = "2012";

		/** Declare the filters **/
		Filter filter1 = new PrefixFilter(rowId.getBytes());
		Filter gteFilter = new QualifierFilter(CompareOp.GREATER_OR_EQUAL, new BinaryComparator(qualifier.getBytes()));
		Filter gtFilter = new QualifierFilter(CompareOp.GREATER, new BinaryComparator(qualifier.getBytes()));
		Filter lteFilter = new QualifierFilter(CompareOp.LESS_OR_EQUAL, new BinaryComparator(qualifier.getBytes()));
		Filter ltFilter = new QualifierFilter(CompareOp.LESS, new BinaryComparator(qualifier.getBytes()));
		List<Filter> filters = Arrays.asList(filter1, gtFilter);

		/**** Create Table if not Exist ****/
		/*
		 * boolean result = app.CreateTable(tableName, family);
		 * System.out.println("Table Created? : " + result);
		 */

		/**** Delete Table ****/
		/*
		 * boolean result = app.DeleteTable(tableName);
		 * System.out.println("Table deleted? : " + result);
		 */

		/**** Find and display all Tables ****/
		/*
		 * listTable();
		 */

		/**** Find and display All content from a table ****/
		/*
		 * ResultScanner scanner = app.filter(tableName); if (scanner != null){ for
		 * (Result result : scanner) {
		 * 
		 * byte[] make = result.getValue(Bytes.toBytes("vi"), Bytes.toBytes("make"));
		 * byte[] model = result.getValue(Bytes.toBytes("vi"), Bytes.toBytes("model"));
		 * byte[] year = result.getValue(Bytes.toBytes("vi"), Bytes.toBytes("year"));
		 * 
		 * 
		 * String car_make = Bytes.toString(make); String car_model =
		 * Bytes.toString(model); String car_year = Bytes.toString(year);
		 * 
		 * System.out.println("Car Make: " + car_make + " " + " Car Model: " + car_model
		 * + " " + " Car Year: " + car_year); } }else{
		 * System.out.println("No results Found"); }
		 */

		/**** Find and display on filter from a table ****/
		/*
		 * ResultScanner scanner2 = app.filter(tableName,filters); if (scanner2 != null)
		 * { for (Result result : scanner2) {
		 * 
		 * byte[] make = result.getValue(Bytes.toBytes("vi"), Bytes.toBytes("make"));
		 * byte[] model = result.getValue(Bytes.toBytes("vi"), Bytes.toBytes("model"));
		 * byte[] year = result.getValue(Bytes.toBytes("vi"), Bytes.toBytes("year"));
		 * 
		 * 
		 * String car_make = Bytes.toString(make); String car_model =
		 * Bytes.toString(model); String car_year = Bytes.toString(year);
		 * 
		 * System.out.println("Car Make: " + car_make + " " + " Car Model: " + car_model
		 * + " " + " Car Year: " + car_year); } }else{
		 * System.out.println("No results Found: "); }
		 */

		/**** Update ****/
		/*
		 * boolean result = app.update(tableName, rowId, family, qualifier, new_value);
		 * System.out.println("Updated? : " + result);
		 */

		/**** Delete from table ****/
		/*
		 * // Deleting row HBase boolean result = app.delete(tableName, rowId, family,
		 * qualifier); System.out.println("Deleted? : " + result);
		 */

		app.disconnectHBase();
	}
}
