# M.Sc NoSql APIs
The M.Sc Big Data Repository.  

The M.Sc Big Data Repository includes three (3) directories.
   
##Directory 1 - Containers
Directory "Containers" includes the files need to run two (2) containers in docker environment. The first container would host
MongoDB and the second one Hbase.
 
##Directory 2 - DataForDbs  
Directory "DataForDbs" includes a txt with commands for manually adding data into Hbase database and a direcotry with a python script
in order to add automatically data into MongoDB from a json file.

##Directory 3 - Java Project     
Directory "Java Project" includes a maven java project in order to test the implemented APIs

## Prerequisites to run locally
Before moving on, make sure you have also installed Apache Maven Project the latest and Docker

Install Apache Maven Project
```sh
  apt-cache search maven
  sudo apt-get install maven
```

Install Docker
```sh
  sudo apt-get update
  sudo apt-get install docker-ce
  
```

Verify that Docker CE is installed correctly by running the hello-world image.
```sh
  sudo docker run hello-world 
```

## Usage
The following shows how to run the Nosql Apis Project:      
* First, build the images of the containers and run them. Use the following commands:     
	** cd hbase-cont      
	** docker build -t debian-hbase.       
	** docker run -d -h myhbase -p 2181:2181 -p 60000:60000 -p 60010:60010 -p 60020:60020 -p 60030:60030 --name pyke debian-hbase    
	** docker-compose up    
* For the hbase to runn properly you have to add the following in your /etc/hosts file    
	** <host_machine_ip> myhbase    

* After the above steps you have to run the maven project    
	** Use any Java IDE (Eclipse ise the preferred one)    
	** Import existing maven project    
	** Choose a class to run first (MongoConnectorApp.java/HBaseConnectorApp.java)    

## API Documentation    

The API classes implemented, among with their methods are the following:    
### ConnectorApp.java    
**Connecto to MongoDB**        
public boolean connectMongo(String host, int port, String dbname);   
OR     
connectMongo(String url);       
**Connecto to HBase**     
connectHBase(String host, int port, String path);          
**Close MongoDb's Connection**     
 public void disconnectMongo();         
**Close Hbase's Connection**     
public void disconnectHBase();         
**Get if Mongo is Connected**     
public boolean isMongoConnected();         
**Set MongoDb connected**     
public void setMongoConnected(boolean isMongoConnected);         
**Get if HBase is connected**    	
public boolean isHBaseConnected();     
**Set HBase to connected**     
public void setHBaseConnected(boolean isHBaseConnected);         

### MongoConnectorApp     
**Connect to Mongo DB**    
connectMongo(host, port, dbname);        
**Insert data in Collection**    
AddDataToCollection(dbname,collectionName,document);    
**Find and display All**    
filter(collectionName);    
**Find and display All collections from db**    
collectionNames(dbname);        
**Find and display on filter**    
filter(collectionName, filter);     
**Update**    
Bson update = set("record", "This is a Test");     
update(collectionName, filter, update);     
**Delete**    
delete(collectionName, filter);       
**Create Database with Collection**     
createDatabaseWithCollection(dbname, collectionName));     

### HBaseConnectorApp         
**Connect to Hbase**    
connectHBase(host, port, path);    
**Create Table if not Exist**    
CreateTable(tableName, family);    
**Delete Table**    
DeleteTable(tableName);    
**Find and display all Tables**    
listTable();    
**Find and display All content from a table**    
filter(tableName);    
**Find and display on filter from a table**    
filter(tableName,filters);    
**Update**    
update(tableName, rowId, family, qualifier, new_value);    
**Delete from table**    
delete(tableName, rowId, family, qualifier);    
  
### Contributing

You may contribute to the M.Sc NoSql APIs repository similar to other (sub-) projects, i.e. by creating pull requests.    


#### Lead Developer

The following developer is responsible for this repository and have admin rights. He can, for example, merge pull requests.    
*  Marios Touloupou (@mtouloup)



